import express from 'express';

import { getUserByEmail, verifyToken } from '../middleware/auth.middleware.js';

import { register, login, logout } from '../Controllers/auth.controllers.js';

const router = express.Router();

router.post('/register', register);
router.post('/login', getUserByEmail, login);
router.post('/logout', verifyToken, logout);

/**
 * Future enhancements -
 * 1. Enable refresh token feature
 * 2. Forget password feature
 * 3. Email Verification feature
 */

export { router as AuthRoute };
