import express from 'express';

import {
  getAll,
  create,
  update,
  deleteAll,
} from '../Controllers/userLikes.controllers.js';

import { getCityById } from '../middleware/cities.middleware.js';
import {
  getUserLikes,
  populateUserLikes,
} from '../middleware/userLikes.middleware.js';

const router = express.Router();

//Get All
router.get('/', populateUserLikes, getAll);

//Create record
router.post('/', create);

//Update ( Body params: city_to_add, city_to_delete )
router.patch('/', getUserLikes, getCityById, update);

//Delete All Records
router.delete('/', getUserLikes, deleteAll);

export { router as userLikesRoute };
