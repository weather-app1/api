import express from 'express';
import { getWeather } from '../Controllers/weather.controllers.js';

const router = express.Router();

router.get('/', getWeather);

export { router as weatherRoute };
