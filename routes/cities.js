import express from 'express';

import {
  getCityById,
  getCityByParams,
} from '../middleware/cities.middleware.js';

import {
  getAll,
  getOneById,
  create,
  update,
  deleteCity,
} from '../Controllers/cities.controllers.js';

import { verifyToken } from '../middleware/auth.middleware.js';

const router = express.Router();

router.get('/', getCityByParams, getAll); //Get All
router.get('/:id', getOneById); //Get One By Id
router.post('/', verifyToken, create); //Create
router.patch('/:id', verifyToken, getCityById, update); //Update
router.delete('/:id', verifyToken, getCityById, deleteCity); //Delete

export { router as citiesRoute };
