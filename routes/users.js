import express from 'express';

import { verifyToken } from '../middleware/auth.middleware.js';

const router = express.Router();

router.get('/', verifyToken, (req, res) => {
  res.status(200).json({ data: req.user });
});

export { router as UsersRoute };
