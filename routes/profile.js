import express from 'express';
import { getProfile } from '../Controllers/profile.controllers.js';

const router = express.Router();

router.get('/', getProfile); //Get Profile

export { router as profileRoute };
