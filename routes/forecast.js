import express from 'express';

import { get5Days } from '../Controllers/forecast.controllers.js';

const router = express.Router();

router.get('/', get5Days); //Get 5 Days Forecast

export { router as forecastRoute };
