import express from 'express';
import unirest from 'unirest';

const router = express.Router();

router.get('/', async (req, res) => {
  const currentLocation = unirest(
    'GET',
    'https://ip-geolocation-ipwhois-io.p.rapidapi.com/json/'
  );

  currentLocation.headers({
    'x-rapidapi-host': process.env.RAPID_API_HOST,
    'x-rapidapi-key': process.env.RAPID_API_KEY,
  });
  currentLocation.end(function (result) {
    if (res.error) throw new Error(result.error);

    const data = {
      city: result.body.city,
      country: result.body.country,
      iso2: result.body.country_code,
      latitude: result.body.latitude,
      longitude: result.body.longitude,
    };
    res.status(200).json({ data: data, status: 'Request Successful' });
  });
});

export { router as currentLocationRoute };
