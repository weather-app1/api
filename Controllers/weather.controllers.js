import axios from 'axios';

import { handleFormRequestSuccess, throwError } from '../Utils/formRequest.js';

//Get Weather (Query Params: (city_name))
async function getWeather(req, res) {
  const apiLink = `https://api.openweathermap.org/data/2.5/find?q=${req.query.city_name}&units=metric&appid=${process.env.OPEN_WEATHER_API_KEY}`;
  try {
    const {
      data: { list },
    } = await axios.get(apiLink);

    handleFormRequestSuccess(res, 'Request Successful', list[0]);
  } catch (error) {
    throwError(res, 400, 'Unable to retrieve weather data.');
  }
}

export { getWeather };
