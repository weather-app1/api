import axios from 'axios';
import { handleFormRequestSuccess, throwError } from '../Utils/formRequest.js';

async function get5Days(req, res) {
  const apiLink = `https://api.openweathermap.org/data/2.5/forecast?q=${req.query.city}&units=metric&appid=${process.env.OPEN_WEATHER_API_KEY}`;

  try {
    const {
      data: { list },
    } = await axios.get(apiLink);
    const data = [list[0], list[8], list[16], list[24], list[32]];

    handleFormRequestSuccess(res, 'Request Successful.', data);
  } catch (error) {
    throwError(res, 400, 'Failed to retrieve forecast data.');
  }
}

export { get5Days };
