import bcrypt from 'bcrypt';
import userModel from '../models/users.js';
import {
  handleFormRequestSuccess,
  handleFormRequestError,
  throwError,
} from '../Utils/formRequest.js';

import { generateAccessToken } from '../middleware/auth.middleware.js';

async function register(req, res) {
  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);

    const user = new userModel({
      username: req.body.username,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      password: hashedPassword,
    });

    await user.save();
    handleFormRequestSuccess(res, 'Account created successfully.');
  } catch (error) {
    handleFormRequestError(
      res,
      error,
      'This email is invalid or has been used.'
    );
  }
}

async function login(req, res) {
  if (await bcrypt.compare(req.body.password, res.user.password)) {
    const user = { email: res.user.email };
    const access_token = generateAccessToken(user);
    handleFormRequestSuccess(res, 'Login Successful', { access_token });
  } else {
    throwError(res, 400, 'Invalid username or password');
  }
}

function logout(req, res) {
  res
    .status(200)
    .cookie('auth._token.local', '')
    .json({ status_code: 200, status_message: 'Log out successful.' });
}

export { register, login, logout };
