import {
  handleFormRequestSuccess,
  handleFormRequestError,
  throwError,
} from '../Utils/formRequest.js';
import UserLike from '../models/userLike.js';
import userModel from '../models/users.js';

function getAll(req, res) {
  handleFormRequestSuccess(res, 'Request Successful.', res.data);
}

async function create(req, res) {
  const userArray = await userModel.find({ email: req.user.email });
  const userData = userArray[0];

  try {
    const payload = await new UserLike({
      _id: userData._id,
    });
    const newRecord = await payload.save();
    handleFormRequestSuccess(res, 'Successfully created.', newRecord);
  } catch (error) {
    handleFormRequestError(res, error, 'Record already existed.');
  }
}

async function update(req, res) {
  if (req.body.city_to_add)
    res.userLike.liked_cities.addToSet(req.body.city_to_add);
  else res.userLike.liked_cities.pull(req.body.city_to_delete);

  try {
    const update = await res.userLike.save();
    handleFormRequestSuccess(res, 'Successfully updated.', update);
  } catch (error) {
    handleFormRequestError(res, error, 'Failed to update data.');
  }
}

async function deleteAll(req, res) {
  try {
    await res.userLike.remove();
    handleFormRequestSuccess(res, 'Deleted all records.');
  } catch (error) {
    throwError(res, 400, 'Failed to delete record.');
  }
}

export { getAll, create, update, deleteAll };
