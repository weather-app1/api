import { cityModel } from '../models/city.js';
import { createPaginationData } from '../Utils/pagination.js';

import {
  handleFormRequestSuccess,
  handleFormRequestError,
  throwError,
} from '../Utils/formRequest.js';

async function getAll(req, res) {
  const cities = res.cities.docs;
  const pagination = createPaginationData(res.cities);

  handleFormRequestSuccess(res, 'Request Successful', {
    records: cities,
    pagination,
  });
}

async function getOneById(req, res) {
  let city;
  const id = req.params.id || req.body.city_to_add || req.body.city_to_delete;

  try {
    city = await cityModel.findById(id);

    handleFormRequestSuccess(res, 'Request Successful', city);
  } catch (error) {
    throwError(res, 400, 'Cannot find city.');
  }
}

async function create(req, res) {
  const city = new cityModel({
    city: req.body.city,
    country: req.body.country,
    iso2: req.body.iso2,
    admin_name: req.body.admin_name,
    capital: req.body.capital,
    population: req.body.population,
  });

  try {
    const newCity = await city.save();
    handleFormRequestSuccess(res, 'Successfully created!', newCity);
  } catch (error) {
    handleFormRequestError(
      res,
      error,
      'Failed to create city. Please try again later.'
    );
  }
}

async function update(req, res) {
  const keys = Object.keys(req.body);
  keys.forEach((key) => {
    if (req.body[key] != null) res.city[key] = req.body[key];
  });

  try {
    const updatedCity = await res.city.save();
    handleFormRequestSuccess(res, 'Successfully updated.', updatedCity);
  } catch (error) {
    handleFormRequestError(
      res,
      error,
      'Failed to update data. Pleas try again later.'
    );
  }
}

async function deleteCity(req, res) {
  try {
    await res.city.remove();
    handleFormRequestSuccess(res, 'Successfully deleted.');
  } catch (error) {
    handleFormRequestError(
      res.error,
      'Failed to delete city. Please try again later.'
    );
  }
}

export { getAll, getOneById, create, update, deleteCity };
