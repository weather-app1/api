import userModel from '../models/users.js';
import { handleFormRequestSuccess, throwError } from '../Utils/formRequest.js';

async function getProfile(req, res) {
  try {
    const userArray = await userModel.find({ email: req.user.email });
    const user = userArray[0];

    const userData = {
      email: user.email,
      username: user.username,
      first_name: user.first_name,
      last_name: user.last_name,
      verified: user.verified,
      profile_photo: user.profile_photo,
    };

    handleFormRequestSuccess(res, 'Request Seccessful', userData);
  } catch (error) {
    throwError(res, 400, 'Unable to get profile.');
  }
}

export { getProfile };
