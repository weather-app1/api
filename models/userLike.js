import mongoose from 'mongoose';
const { Schema } = mongoose;

const userLikeSchema = new Schema({
  _id: { type: String, required: true },
  liked_cities: [{ type: Schema.Types.ObjectId, ref: 'City' }],
});

export default mongoose.model('Userlike', userLikeSchema, 'userlikes');
