import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const { Schema } = mongoose;

const citySchema = new Schema({
  city: { type: String, required: true },
  country: { type: String, required: true },
  iso2: { type: String, required: false },
  admin_name: { type: String, required: true },
  capital: { type: String, required: false },
  population: { type: Number, required: false },
});

citySchema.plugin(mongoosePaginate);
const cityModel = mongoose.model('City', citySchema, 'cities');

export { citySchema, cityModel };
