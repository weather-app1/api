import express from 'express';
import dotenv from 'dotenv';
import allowCors from './mixins/allowCors.mixins.js';
import initDatabase from './config/mongoose.config.js';
import { verifyToken } from './middleware/auth.middleware.js';

import {
  AuthRoute,
  citiesRoute,
  currentLocationRoute,
  profileRoute,
  userLikesRoute,
  weatherRoute,
  forecastRoute,
} from './routes/index.js';

const version = 'v1'
const basePath = `/api/${version}`

const {config} = dotenv

config();
const app = express();
const PORT = process.env.PORT;
app.disable('etag');

allowCors(app); //Allow CORS
initDatabase();
app.use(express.json());

app.listen(PORT, () => {
  console.log(`Server is running...`);
});

app.get('/', (req, res) => {
  res.send('Server is running...');
});

//Enable routes
app.use(`${basePath}/auth`, AuthRoute);
app.use(`${basePath}/cities`, citiesRoute);
app.use(`${basePath}/currentLocation`, currentLocationRoute);
app.use(`${basePath}/forecast`, forecastRoute);
app.use(`${basePath}/profile`, verifyToken, profileRoute);
app.use(`${basePath}/userLikes`, verifyToken, userLikesRoute);
app.use(`${basePath}/weather`, weatherRoute);
