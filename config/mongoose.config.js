import mongoose from 'mongoose';

export default function initDatabase() {
  const connectUri = `mongodb+srv://${process.env.MONGOOSE_USERNAME}:${process.env.MONGOOSE_PASSWORD}@cluster0.xoxkz.mongodb.net/${process.env.MONGOOSE_DBNAME}?retryWrites=true&w=majority`;

  mongoose.connect(connectUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const db = mongoose.connection;

  db.on('error', console.error.bind(console, 'connection error: '));

  db.once('open', () => {
    console.log('Connected to database');
  });
}
