import jwt from 'jsonwebtoken';
import userModel from '../models/users.js';

import { throwError } from '../Utils/formRequest.js';

function verifyToken(req, res, next) {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (token == null) return res.sendStatus(401);

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(401);
    req.user = user;
    next();
  });
}

function generateAccessToken(user) {
  return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
}

async function getUserByEmail(req, res, next) {
  let user;

  try {
    user = await userModel.find({ email: req.body.email });
    if (user.length == 0) {
      return throwError(res, 400, 'User does not exist.');
    }
  } catch (error) {
    return throwError(res, 500, 'Unknown server error.');
  }

  res.user = user[0];
  next();
}

export { verifyToken, getUserByEmail, generateAccessToken };
