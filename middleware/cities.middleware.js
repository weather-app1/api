import { cityModel } from '../models/city.js';

import { throwError } from '../Utils/formRequest.js';
import { paginationOption } from '../Utils/pagination.js';

async function getCityById(req, res, next) {
  let city;
  const id = req.params.id || req.body.city_to_add || req.body.city_to_delete;
  try {
    city = await cityModel.findById(id);
    res.city = city;
    next();
  } catch (error) {
    return throwError(res, 400, 'Cannot find city.');
  }
}

async function getCityByParams(req, res, next) {
  const options = paginationOption(req.query.page);
  let cities;
  let query = {};

  if (req.query.city) query = { city: new RegExp(`${req.query.city}`, 'gi') };

  try {
    cities = await cityModel.paginate(query, options);
  } catch (error) {
    return throwError(res, 400, 'No data available.');
  }

  res.cities = cities;
  next();
}

export { getCityById, getCityByParams };
