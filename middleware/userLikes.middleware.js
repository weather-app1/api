import userModel from '../models/users.js';
import { throwError } from '../Utils/formRequest.js';
import UserLike from '../models/userLike.js';

async function getUserLikes(req, res, next) {
  try {
    const userArray = await userModel.find({ email: req.user.email });
    const userData = userArray[0];

    const userLike = await UserLike.findById(userData._id);
    res.userLike = userLike;
    next();
  } catch (error) {
    return throwError(res, 400, 'No record found.');
  }
}

async function populateUserLikes(req, res, next) {
  try {
    const userArray = await userModel.find({ email: req.user.email });
    const userData = userArray[0];

    const populate = await UserLike.findById(userData._id).populate({
      path: 'liked_cities',
    });

    if (populate == null) return throwError(res, 400, 'No record found.');

    res.data = populate;
    next();
  } catch (error) {
    return throwError(res, 400, 'No record found.');
  }
}

export { populateUserLikes, getUserLikes };
