function handleFormRequestSuccess(res, message, data = null) {
  res.status(200).json({
    status_code: 200,
    status_message: message,
    ...(data != null && { data: data }),
  });
}

function handleFormRequestError(res, error, duplicateMessage = '') {
  if (error.name == 'ValidationError') {
    res.status(400).json({
      status_code: 400,
      status_message: error._message,
      data: { formErrors: error.errors },
    });
  } else if (error.code == 11000) {
    //Duplicate item
    res.status(400).json({
      status_code: 400,
      status_message: duplicateMessage,
    });
  } else {
    res.status(500).json({
      status_code: 500,
      status_message: error.message || 'Unknown server error',
    });
  }
}

function throwError(res, errorCode, errorMessage) {
  res
    .status(errorCode)
    .json({ status_code: errorCode, status_message: errorMessage });
}

export { handleFormRequestSuccess, handleFormRequestError, throwError };
