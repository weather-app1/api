function createPaginationData(item) {
  return {
    current_page: item.page,
    last_page: item.totalPages,
    page_size: item.limit,
    total_records: item.totalDocs,
    has_more_page: item.hasNextPage,
  };
}

const paginationOption = (page) => {
  return {
    page: page || 1,
    limit: 10,
    collation: { locale: 'en' },
  };
};

export { createPaginationData, paginationOption };
